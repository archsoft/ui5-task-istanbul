
// based on https://github.com/istanbuljs/nyc/blob/master/lib/instrumenters/istanbul.js
const { createInstrumenter } = require('istanbul-lib-instrument')
const convertSourceMap = require('convert-source-map')
const log = require("@ui5/logger").getLogger("builder:customtask:istanbul");

/**
 * Task to transpiles ES6 code into ES5 code.
 *
 * @param {Object} parameters Parameters
 * @param {DuplexCollection} parameters.workspace DuplexCollection to read and write files
 * @param {AbstractReader} parameters.dependencies Reader or Collection to read dependency files
 * @param {Object} parameters.options Options
 * @param {string} parameters.options.projectName Project name
 * @param {string} [parameters.options.configuration] Task configuration if given in ui5.yaml
 * @returns {Promise<undefined>} Promise resolving with undefined once data has been written
 */
// eslint-disable-next-line no-unused-vars
module.exports = function ({ workspace, dependencies, options }) {
    const instrumenter = createInstrumenter({
        autoWrap: true,
        coverageVariable: '__coverage__',
        embedSource: true,
        compact: options.compact,
        preserveComments: options.preserveComments,
        produceSourceMap: options.produceSourceMap,
        ignoreClassMethods: options.ignoreClassMethods,
        esModules: options.esModules,
        parserPlugins: options.parserPlugins
    })


    return workspace.byGlob("/**/*.js").then((resources) => {
        return Promise.all(resources.map((resource) => {
            if (!(options.configuration && options.configuration.excludePatterns || []).some(pattern => resource.getPath().includes(pattern))) {
                return resource.getString().then((value) => {
                    options.configuration && options.configuration.debug && log.info("Instrument file " + resource.getPath())
                    var instrumented = instrumenter.instrumentSync(value, resource.getPath() /*, sourceMap*/)

                    // the instrumenter can optionally produce source maps,
                    // this is useful for features like remapping stack-traces.
                    if (options.produceSourceMap) {
                        var lastSourceMap = instrumenter.lastSourceMap()
                        /* istanbul ignore else */
                        if (lastSourceMap) {
                            instrumented += '\n' + convertSourceMap.fromObject(lastSourceMap).toComment()
                        }
                    }
                    return instrumented
                }).then((result) => {
                    resource.setString(result)
                    workspace.write(resource)
                })
            } else {
                return Promise.resolve()
            }
        }))
    })
}