# UI5 task for live reloading `any` sources on change

task for [ui5-builder](https://github.com/SAP/ui5-builder), doing instrumentation

## Install

```bash
npm install ui5-task-istanbul --save-dev
```

## Configuration options (in `$yourapp/ui5.yaml`)
see config

## Usage

1. Define the dependency in `$yourapp/package.json`:

```json
"devDependencies": {
    // ...
    "ui5-task-istanbul": "*"
    // ...
},
"ui5": {
  "dependencies": [
    // ...
    "ui5-task-istanbul",
    // ...
  ]
}
```

> As the devDependencies are not recognized by the UI5 tooling, they need to be listed in the `ui5 > dependencies` array. In addition, once using the `ui5 > dependencies` array you need to list all UI5 tooling relevant dependencies.

2. configure it in `$yourapp/ui5.yaml`:

```yaml
specVersion: "1.0"
metadata:
  name: ui5-app
type: application
builder:
  customTasks:
  - name: ui5-task-istanbul
    afterTask: replaceVersion
    configuration:
      debug: true
      excludePatterns:
      - "lib/"
      - "another/dir/in/webapp"
      - "yet/another/dir"
```

## How it works

The task launches a `istanbul`-builder on the specified `port`, listening to changes in the specified `path` inside your application directory.

When changes are detected, a reload is triggered to **all connected clients** - so all browsers having `$yourapp` will reload the application. The reload is `#`-aware, meaning the current displayed route in your single-page UI5 app is kept steady.

## Misc/FAQ

yep, cross-browser, cross-platform.

> UI5 dependencies management is unique... wrapper function could break some complex component projects. Feel free to open an issue

## License

[THE DERIVED BEER-WARE LICENSE](../../LICENSE)
